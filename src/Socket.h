#ifndef SIMPLE_SERVER_SOCKET_H_
#define SIMPLE_SERVER_SOCKET_H_

#include <sys/epoll.h>      // for epoll
#include <unordered_map>    // for std::unordered_map
#include <string>   // for std::string

namespace simple_server {
/**
 * This class can contain info about client (address, statistics etc)
 * But at the moment it's left empty
 */
class ClientInfo{
  public:
    ClientInfo(){};
};

class Socket {
  public:
    Socket(int port);
    virtual ~Socket(){};
    /**
     * Initializes the socket
     */
    bool Init();
    /**
     * Starts to listen on the socket
     */
    bool Start();
  protected:
    /**
     *  Handle socket activity (new clients connection for example)
     */
    void handleEvent();
    /**
     * Handle client(s) activity (disconnect, data receiving)
     */
    void handleClientEvent(int fd);
    //---------------------------------//
    /**
     * This function is called when new client is connected
     */
    void ClientConnectedEvent(int fd);
    /**
     * This function is called when client is disconnecting
     */
    void ClientDisconnectedEvent(int fd);
    /**
     * This is the event handler for data received from a client
     * This is the only function, that needs to be implemented on server side
     */
    virtual void ClientDataReceivedEvent(int fd, const std::string &data){};
    /**
     * This function sends data to a client
     */
    void ClientDataSend(int fd, const std::string &data);
    /**
     * This function broadcasts data to all connected clients
     */
    void ClientDataSend(const std::string &data);

  protected:
    std::unordered_map<int, ClientInfo> fClients; // List of connected clients (for broadcasting)
    int fSock;  // The socket ID
    int fPort;  // The listening port
    int fEpoll; // The epoll ID
    const int cMAX_EVENTS = 1024;       // Length of epoll events array
    struct epoll_event fEvents[1024];   // epoll events
};
}
#endif // SIMPLE_SERVER_SOCKET_H_
