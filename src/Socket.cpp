#include <sys/socket.h> // for socket
#include <arpa/inet.h>  // for sockaddr_in
#include <sys/epoll.h>  // for epoll
#include <fcntl.h>      // for fcntl
#include <unistd.h>     // for close
#include <cerrno>       // for errno
#include <vector>       // for std::vector
#include <iostream>     // for std::cout

#include "Socket.h"

namespace simple_server {
//-------------------------------------------------------------------//
Socket::Socket(int port): fPort(port) {
}
//-------------------------------------------------------------------//
bool Socket::Init(){
    //---> Initializing the socket :
    if ( (fSock=socket(AF_INET, SOCK_STREAM, 0))==0  ) {
        // Failed to create socket
        return false;
    }
    // Set listening socket to be Non-blocking
    int socket_options;
    socket_options = fcntl(fSock, F_GETFL);
    if (socket_options < 0) return false;
    socket_options |= O_NONBLOCK;
    if ( fcntl(fSock, F_SETFL, socket_options) < 0 ) return false;
    if ( setsockopt(fSock, SOL_SOCKET, SO_REUSEADDR, &socket_options, sizeof(socket_options)) == -1 ) {
        // This should never happen, we can't set socket options...
        return false;
    }
    //---> Initializing the port: setup ANY address...
    struct sockaddr_in address;
    int port = fPort;
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(port);
    if (port == 0) {
        // The Port wasn't set for some reason, this is no-go
        return false;
    }
    //---> Binding the two together:
    if (  bind(fSock, (struct sockaddr *) &address, sizeof(address)) != 0) {
        // This should also never happen, but we're unable to bind socket into the port
        return false;
    }
    return true;
}
//-------------------------------------------------------------------//
bool Socket::Start(){
    if (listen(fSock, SOMAXCONN) < 0) {
        // Failed  to start listening...
        return false;
    }
    if ((fEpoll = epoll_create1(0)) < 0) {
        // Should never happen, can't create epoll instance
        return false;
    }
    // Configure listening socket for all epoll operations
    struct epoll_event ev;
    ev.data.fd = fSock;
    ev.events = EPOLLIN | EPOLLPRI | EPOLLERR | EPOLLHUP | EPOLLET;
    if ( epoll_ctl(fEpoll, EPOLL_CTL_ADD, fSock, &ev) < 0) {
        // Failed to configure epoll
        return false;
    }
    while ( true) {
        // Get number of file descriptors that are ready for I/O
        int nfds = epoll_wait(fEpoll, fEvents, cMAX_EVENTS, -1 /*Timeout*/);
        if (nfds == -1) {
            // Again should never happen
            close(nfds);
            close(fSock);
            return false;
        }
        // Iterate over all the events
        for (auto ii = 0; ii < nfds; ++ii) {
            if (fEvents[ii].data.fd == fSock) {
                // This is our socket
                handleEvent();
            } else {
                // Client's event
                handleClientEvent(ii);
            }
        }
    }
    return true;
}
//-------------------------------------------------------------------//
void Socket::handleEvent(){

    // Accept client connection
    struct sockaddr_in address;
    socklen_t addressLen = sizeof(address);
    int fd = accept(fSock, (struct sockaddr *) &address, &addressLen);
    if (fd < 0) {
        // again should never happen
        return;
    }
    // Set client socket to be Non-blocking
    int socket_options;
    socket_options = fcntl(fd, F_GETFL);
    if (socket_options < 0) {
        return;
    }
    socket_options |= O_NONBLOCK;
    if ( fcntl(fd, F_SETFL, socket_options) < 0 ){
        return;
    }
    // Set edge-triggered
    struct epoll_event ev;
    ev.events = EPOLLIN | EPOLLOUT | EPOLLET;
    ev.data.fd = fd;
    // Add fd to epoll data structure
    if (epoll_ctl(fEpoll, EPOLL_CTL_ADD, fd, &ev) < 0) {
        // should not happen
        return;
    }
    // New client connected
    ClientConnectedEvent(fd);
    // std::cout << "...Client Connected FD=" << fd << std::endl;
}
//-------------------------------------------------------------------//
void Socket::handleClientEvent(int fd){
    if (fEvents[fd].events & EPOLLIN) {
        if ( fEvents[fd].data.fd < 0) {
            // Invalid file descriptor
            return;
        }
        // Data available for reading
        char buffer[512];
        ssize_t result = recv(fEvents[fd].data.fd, buffer, sizeof(buffer), 0);

        if (result < 0) {
            if (errno == ECONNRESET) {
                // Should never happen, failed to recieve data
                close(fEvents[fd].data.fd);
            }
            return;
        } else if (result == 0) {
            close(fEvents[fd].data.fd);
            ClientDisconnectedEvent(fEvents[fd].data.fd);
            // std::cout << "...Client Disconnected FD=" << fEvents[fd].data.fd << std::endl;
        } else {
            struct epoll_event ev;
            ev.events = EPOLLOUT | EPOLLIN | EPOLLET;
            std::string data(buffer, buffer + result);
            ClientDataReceivedEvent(fEvents[fd].data.fd, data);
            // std::cout << "...Data received from FD=" << fEvents[fd].data.fd << std::endl;
        }
    }
    if (fEvents[fd].events & EPOLLOUT) {
        // std::cout << "...EPOLLOUT is not implemented" << std::endl;
    }
    if (fEvents[fd].events & EPOLLRDHUP) {
        // std::cout << "...EPOLLRDHUP is not implemented" << std::endl;
    }
    if (fEvents[fd].events & EPOLLHUP) {
        // std::cout << "...EPOLLHUP is not implemented" << std::endl;
    }
    if (fEvents[fd].events & EPOLLERR) {
        // There was an error
        close(fEvents[fd].data.fd);
    }
}
//-------------------------------------------------------------------//
void Socket::ClientConnectedEvent(int fd){
    fClients.insert({fd, ClientInfo()});
}
//-------------------------------------------------------------------//
void Socket::ClientDisconnectedEvent(int fd){
    auto it=fClients.find(fd);
    if (it == fClients.end()) {
        // Hmm... should never happen
        return;
    }
    fClients.erase(it);
}
//-------------------------------------------------------------------//
void Socket::ClientDataSend(int fd, const std::string &data){
    // std::cout << "   [" << fd << "]>" << data;
   ssize_t result = send(fd, data.c_str(), data.size(), 0);
}
//-------------------------------------------------------------------//
void Socket::ClientDataSend(const std::string &data){
    for (auto it=fClients.cbegin(); it!=fClients.cend(); ++it) {
        ClientDataSend(it->first, data);
    }
}
//-------------------------------------------------------------------//
} // namespace simple_server
