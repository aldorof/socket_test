#include "Server.h"

#include <iostream> // for std::cout
#include <sstream>  // for std::ostringstream

namespace simple_server {
//-------------------------------------------------------------------//
Server::Server(int port): Socket::Socket(port), fCounter(0) {
}
//-------------------------------------------------------------------//
Server::~Server(){
}
//-------------------------------------------------------------------//
void Server::ClientDataReceivedEvent(int fd, const std::string &data) {
    std::cout << "...[" << fd << "]>" << data; 
    // Let's echo it back to the client for a check
    // ClientDataSend(fd, data);

    // Though handling all possible commands in single function is not a good
    // practice, but for the sake of time, let's do it
    std::size_t found = data.find("INCR");
    if (found != std::string::npos ) {
        // Seems like Increment command
        std::istringstream valstr( data.substr(4, data.length() - 5) );
        long long incr = 0;
        valstr >> incr;
        if ( valstr.fail() ) {
            // There was a conversion error
            return;
        }
        fCounter += incr;
        // We want to broadcast on INCR
        std::ostringstream response;
        response << fCounter << std::endl;
        ClientDataSend(response.str());
        return;
    }
    found = data.find("DECR");
    if (found != std::string::npos ) {
        // Seems like Decrement command
        std::istringstream valstr( data.substr(4, data.length() - 5) );
        long long decr = 0;
        valstr >> decr;
        if ( valstr.fail() ) {
            // There was a conversion error
            return;
        }
        fCounter -= decr;
        // We want to broadcast on DECR
        std::ostringstream response;
        response << fCounter << std::endl;
        ClientDataSend(response.str());
        return;
    }
    found = data.find("OUTPUT");
    if (found != std::string::npos ) {
        // Seems like Output command
        std::ostringstream response;
        response << fCounter << std::endl;
        ClientDataSend(fd, response.str());
        return;
    }
    // Do nothing for unknown command
    return;
}
//-------------------------------------------------------------------//
} // namespace simple_server
