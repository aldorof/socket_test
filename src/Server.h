#ifndef SIMPLE_SERVER_SERVER_H_
#define SIMPLE_SERVER_SERVER_H_
#include "Socket.h"

namespace simple_server {
class Server : public Socket {
  public:
    Server(int port);
    ~Server();
  private:
    void ClientDataReceivedEvent(int fd, const std::string &data);
  private:
    long long fCounter;
};
}
#endif // SIMPLE_SERVER_SERVER_H_
