#include <iostream> // for cout
#include <csignal> // for signal
#include <unistd.h> // for sleep
#include <signal.h> // for SIGTERM handling

#include "Server.h"

//-------------------------------------------------------------------//
void sig_hand( int sig_code ) {
    std::cout << std::endl << ">>> Interrupted with " << sig_code << " code" << std::endl;
    exit(0);
}
//-------------------------------------------------------------------//
int main ( int argc, char *argv[] ) {

    simple_server::Server server(8089);

     // Signals will need to be binded to the server eventually
    signal( SIGINT, sig_hand); // To also test with ctrl-c
    signal(SIGTERM, sig_hand); // To test with kill


    if ( !server.Init() ) {
        // No need to continue if we can't init the socket
        return 1;
    }
    if ( !server.Start() ){
        // We start infinite listen cycle here ...
        return 2;
    }
    return 3;
}
//-------------------------------------------------------------------//
