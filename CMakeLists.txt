cmake_minimum_required(VERSION 3.10)

set(CMAKE_CXX_STANDARD 14)

project(SimpleServer LANGUAGES CXX)
set (PROJECT_VERSION "1.0")

set(Project_Sources
    src/simple_server.cpp
    src/Socket.cpp
    src/Server.cpp
)

set(Project_Headers
    src/Socket.h
    src/Server.h
)
#---> This example should explicitly run on Linux only
add_executable( simple_server
    ${Project_Sources} ${Project_Headers})
#---> Packaging will not be needed
